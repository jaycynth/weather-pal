package com.julie.weatherpal.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.julie.weatherpal.data.repositores.WeatherDataRepo

class WeatherDataViewmodel @ViewModelInject constructor(
        private val weatherDataRepo: WeatherDataRepo,
) : ViewModel() {
    fun getWeatherData(cityName: String) = weatherDataRepo.getWeatherData(cityName)
}