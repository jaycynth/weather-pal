package com.julie.weatherpal.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.julie.weatherpal.adapter.CityAdapter
import com.julie.weatherpal.adapter.CityItemClickListener
import com.julie.weatherpal.data.model.City
import com.julie.weatherpal.data.model.cities
import com.julie.weatherpal.databinding.FragmentCitiesFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CitiesFragment : Fragment(), CityItemClickListener {
    private lateinit var binding: FragmentCitiesFragmentBinding
    private lateinit var adapter: CityAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCitiesFragmentBinding.inflate(inflater, container, false)
        requireActivity().title = "Cities"
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        adapter = CityAdapter(cities, this)
        binding.citiesRv.adapter = adapter
    }

    override fun onCityItemClicked(city: City) {
        val action = CitiesFragmentDirections.actionCitiesFragmentToWeatherDataFragment(city.name)
        findNavController().navigate(action)
    }
}