package com.julie.weatherpal.ui

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import com.julie.weatherpal.R
import com.julie.weatherpal.databinding.FragmentWeatherDataBinding
import com.julie.weatherpal.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeatherDataFragment : Fragment() {

    private lateinit var binding: FragmentWeatherDataBinding
    private val viewModel: WeatherDataViewmodel by viewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentWeatherDataBinding.inflate(inflater, container, false)
        requireActivity().title = "Daily Summary"
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val cityName = WeatherDataFragmentArgs.fromBundle(requireArguments()).cityName
        binding.cityName.text = cityName

        viewModel.getWeatherData(cityName).observe(viewLifecycleOwner, {
            when (it.status) {
                Resource.Status.SUCCESS -> {
                    binding.weatherDataProgress.visibility = View.GONE
                    binding.weatherDataLayout.visibility = View.VISIBLE
                    if (!it.data?.weather.isNullOrEmpty()) {
                        binding.weather.text = it.data?.weather?.get(0)?.main

                                binding.weatherDescription.text = it.data?.weather?.get(0)?.description
                        when (it.data?.weather?.get(0)?.main) {
                            "Clear" -> {
                                binding.weatherImage.setImageResource(R.drawable.calm)
                            }
                            "Rain" -> {
                                binding.weatherImage.setImageResource(R.drawable.rainy)

                            }
                            "Clouds" -> {
                                binding.weatherImage.setImageResource(R.drawable.cloudy)

                            }
                            "Wind" -> {
                                binding.weatherImage.setImageResource(R.drawable.windy)

                            }
                        }
                    }
                    val mainData = it.data?.main
                    binding.temprature.text = "Real feel: " + mainData?.feels_like.toString()
                    binding.currentTemprature.text = mainData?.temp.toString() + getString(R.string.degrees)
                    binding.maxTemprature.text = mainData?.temp_max.toString() + getString(R.string.degrees)
                    binding.minTemprature.text = mainData?.temp_min.toString() + getString(R.string.degrees)
                    binding.pressure.text = mainData?.pressure.toString() + "Pa"
                    binding.humidity.text = "Humidity : " + mainData?.humidity.toString() + "%"
                    binding.windSpeed.text = "Wind: " + it.data?.wind?.speed.toString() + "km/h"

                    binding.country.text = it.data?.sys?.country
                }
                Resource.Status.ERROR -> {
                    binding.weatherDataProgress.visibility = View.GONE
                    binding.weatherDataLayout.visibility = View.VISIBLE
                    Toast.makeText(requireActivity(), it.message, Toast.LENGTH_SHORT).show()
                }
                Resource.Status.LOADING -> {
                    binding.weatherDataProgress.visibility = View.VISIBLE
                    binding.weatherDataLayout.visibility = View.GONE
                }
            }
        })
    }
}