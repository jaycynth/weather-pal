package com.julie.weatherpal

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WeatherPal: Application() {
}