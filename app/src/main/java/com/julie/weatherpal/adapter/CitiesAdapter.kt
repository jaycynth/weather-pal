package com.julie.weatherpal.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.julie.weatherpal.data.model.City
import com.julie.weatherpal.databinding.CityLayoutBinding

class CityAdapter(
    private val items: List<City>,
    private val mListener: CityItemClickListener
) : RecyclerView.Adapter<CityAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CityLayoutBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(items[position], mListener)


    inner class ViewHolder(private val binding: CityLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: City, mListener: CityItemClickListener) {
            binding.city = item
            binding.cityClickListener = mListener
            binding.executePendingBindings()
        }
    }
}

interface CityItemClickListener {
    fun onCityItemClicked(city: City)
}
