package com.julie.weatherpal.data.remote

import javax.inject.Inject

class WeatherDataRemote @Inject constructor(
    private val weatherService: WeatherService
) : BaseDataSource() {

    suspend fun getWeatherData(cityName:String) = getResult {
        weatherService.getWeatherForCity(cityName) }

}