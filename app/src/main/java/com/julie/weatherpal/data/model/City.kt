package com.julie.weatherpal.data.model

data class City(val id: Int, val name: String)

val cities = listOf(
        City(11, "Nairobi"),
        City(1, "Lisbon"),
        City(2, "Madrid"),
        City(3, "Paris"),
        City(4, "Berlin"),
        City(5, "Copenhagen"),
        City(6, "Rome"),
        City(7, "London"),
        City(8, "Dublin"),
        City(9, "Prague"),
        City(10, "Vienna")
)
