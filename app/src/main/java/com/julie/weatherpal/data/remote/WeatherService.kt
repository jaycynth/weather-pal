package com.julie.weatherpal.data.remote

import com.julie.weatherpal.BuildConfig
import com.julie.weatherpal.data.model.WeatherData
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {

    @GET("weather")
    suspend fun getWeatherForCity(
        @Query("q")cityName:String,
        @Query("appid")key: String = BuildConfig.key

    ): Response<WeatherData>
}