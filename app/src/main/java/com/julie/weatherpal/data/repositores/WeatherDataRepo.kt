package com.julie.weatherpal.data.repositores


import com.julie.weatherpal.data.remote.WeatherDataRemote
import com.julie.weatherpal.utils.performGetOperation
import javax.inject.Inject

class WeatherDataRepo @Inject constructor(
    private val remoteDataSource: WeatherDataRemote,
) {

     fun getWeatherData(cityName: String) = performGetOperation(
        networkCall = { remoteDataSource.getWeatherData(cityName) }
    )

}