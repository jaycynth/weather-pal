package com.julie.weatherpal.data.model

data class Coord(
    val lat: Double,
    val lon: Double
)