package com.julie.weatherpal.data.model

data class Wind(
    val deg: Int,
    val speed: Double
)